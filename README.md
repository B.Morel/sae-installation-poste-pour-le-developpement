Lien vers gitlab : https://gitlab.com/B.Morel/sae-installation-poste-pour-le-developpement.git

- j'ai installé Xubuntu sur mon pc windows via Virtualbox en utilisant un fichier .iso
- j'ai du activer la virtualisation VT-x dans le bios
- après avoir lancé la machine, j'ai installé Virtual Studio Code
- j'ai testé les différentes commandes requises.
    - print("Hello World") en python dans VS code
    - docker run hello-world n'était pas reconnu et demandais d'installer docker avec cette commande : sudo apt-get install docker
    - j'ai créé un fichier java avec les instructions de la consigne, puis je l'ai compilé avec la commande javac. 
      Celle ci non plus n'était pas reconnue et je l'ai donc installé avec :  sudo apt-get install
